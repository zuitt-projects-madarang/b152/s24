//1

	db.users.find({$or:[{"firstname":{$regex:'y',$options:'$i'}},{"lastname":{$regex:'y',$options:'$i'}}]},{"_id":0,"email":1,"isAdmin":1})

//2
	
	db.users.find({$and:[{"firstname":{$regex:'e',$options:'$i'}},{"isAdmin":true}]},{"_id":0,"email":1,"isAdmin":1})

//3

	db.products.find({$and:[{"name":{$regex:'x',$options:'$i'}},{"price":{$gte:50000}}]})

//4

	db.products.updateMany({"price":{$lt:2000}},{$set:{"isActive":false}})

//5

	db.products.deleteMany({"price":{$gt:20000}})